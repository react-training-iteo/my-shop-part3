import React, {Component} from "react";

import "./App.css";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import ProductList from "./Components/ProductList";
import Cart from "./Components/Cart";



class App extends Component {
  constructor() {
    super();

    this.state = {
      productList: [
        {
          id: Math.random(),
          name: "Sony Play Station 4",
          description: "Poznaj bardziej smukłe, mniejsze PS4, które oferuje graczom niesamowite wrażenia z gier.",
          photo: "https://placeimg.com/140/180/people",
          price: 1399,
          in_stock: true
        },
        {
          id: Math.random(),
          name: "Xbox One X",
          description: "Na Xbox One X gra się jeszcze lepiej. Dzięki większej o 40% mocy niż na jakiejkolwiek innej konsoli.",
          photo: "https://placeimg.com/140/180/nature",
          price: 1500,
          in_stock: true
        },
        {
          id: Math.random(),
          name: "Macbook PRO 2018",
          description: "MacBook Pro - dotyk czyni cuda Jest niewiarygodnie smukły, lekki jak piórko, a do tego potężniejszy i szybszy niż kiedykolwiek",
          photo: "https://placeimg.com/140/180/dog",
          price: 14900,
          in_stock: true
        },
        {
          id: Math.random(),
          name: "Macbook Air 2018",
          description: "11-calowy MacBook Air działa bez ładowania baterii do 9 godzin, a 13-calowy - nawet 12.",
          photo: "https://placeimg.com/140/180/architecture",
          price: 3600,
          in_stock: true
        },
        {
          id: Math.random(),
          name: "iPhone X",
          description: "iPhone X to telefon, który nie tyle posiada ekran, ile po prostu sam tym ekranem jest.",
          photo: "https://placeimg.com/140/180/tech",
          price: 7299,
          in_stock: false
        },
      ],
      filtredData: [],
      myProducts: []
    };
  }

  componentWillMount() {
    this.setState({
      ...this.state,
      filteredData: [...this.state.productList]
    })
  }

  filterProducts = (value) => {
    const newProductList = [...this.state.productList].filter(v => v.name.toLowerCase().startsWith(value.toLowerCase()));

    this.setState({
      ...this.state,
      filteredData: [...newProductList]
    })
  };

  addToCart = (id) => {
    const productInCart =  this.state.productList.find(v => v.id === id)

    this.setState({
      ...this.state,
      myProducts: [...this.state.myProducts, productInCart]
    });
  };

  removeFromCart = (id) => {
    const newMyProducts = [...this.state.myProducts].filter(v => v.id !== id);

    this.setState({
      ...this.state,
      myProducts: [...newMyProducts]
    })
  }

  render() {
    const {myProducts} = this.state;
    return (
      <div className="App">
        <Header/>
        <div className="container">
          <ProductList {...this.state} filterProducts={this.filterProducts} addToCart={this.addToCart}/>
          <Cart products={myProducts} removeFromCart={this.removeFromCart}/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default App;
