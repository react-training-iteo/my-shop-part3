import React from 'react';
import './ProductList.css';
import SearchBox from './Components/SearchBox';
import Product from './Components/Product'

class ProductList extends React.Component {


  getProducts() {
    const {filteredData} = this.props;
    const {addToCart} = this.props;

    return filteredData.map((value, index) => (
      <Product key={index} addToCart={addToCart} id={value.id} name={value.name} description={value.description} photo={value.photo} price={value.price} stock={value.in_stock} />
    ))
  }

  render() {
    const {filterProducts} = this.props
    return (
      <section className="product-list">
        <SearchBox searchText={filterProducts} />
        {this.getProducts()}
      </section>
      )
  }
}
ProductList.defaultProps = {
  productList: []
}

export default ProductList;
