import React from 'react';
import './Cart.css';

import SelectedProduct from './Components/SelectedProducts';
import TotalPrice from './Components/TotalPrice'

class Cart extends React.Component {

  showProductsInCart() {
    const {products} = this.props;
    const {removeFromCart} = this.props;

    return products.map((value, index) => {
      return (
        <SelectedProduct key={index} id={index} productId={value.id} name={value.name} price={value.price} removeFromCart={removeFromCart} />
      )
    })
  }

  sumProductsPrices(){
    const {products} = this.props;
    return products.length > 0 ? products.map((value) => value.price).reduce((a, b) => a + b) : 0
  }

  render() {
    return (
      <section className="user-cart">
        <h4>Twój koszyk</h4>
        <ul className="user-cart--list">
          {this.showProductsInCart()}
        </ul>
        <TotalPrice price={this.sumProductsPrices()}/>
      </section>
      )
  }
}

Cart.defaultProps = {
  products: []
}

export default Cart;
