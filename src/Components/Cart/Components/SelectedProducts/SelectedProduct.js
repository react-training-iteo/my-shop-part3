import React from 'react';
import './SelectedProduct.css';

class SelectedProduct extends React.Component {

  render() {
    const {id, productId, name, price, removeFromCart} = this.props;
    return (
      <li ><button onClick={() => removeFromCart(productId)}>x</button> {id} - {name} - {price} zł</li>
      )
  }
}

export default SelectedProduct;
