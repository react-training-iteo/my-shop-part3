import React from 'react';
import './TotalPrice.css';


class TotalPrice extends React.Component {

  render() {
    const {price} = this.props;

    return (
      <section className="total-price">
       Podsumowanie: {price} zł
      </section>
      )
  }
}

export default TotalPrice;
